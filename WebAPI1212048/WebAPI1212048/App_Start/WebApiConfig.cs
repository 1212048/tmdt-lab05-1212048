﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.WebHost;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Net.Http;
using System.Net.Http.Headers;

namespace WebAPI1212048
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/2.0/{controller}/{action}/{username}",
                defaults: new { username = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/2.0/{controller}/{username}",
               defaults: new { username = RouteParameter.Optional }
           );
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/1212048"));
        }
        
    }
}
