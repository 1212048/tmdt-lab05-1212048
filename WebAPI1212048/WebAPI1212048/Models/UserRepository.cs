﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212048.Models
{
    public class UserRepository :  IUserRepository
    {
        private List<User> user = new List<User>();

        public UserRepository()
        {
            Add1212048(new User { Username = "1212048", Password = "1212048", Category = "TMDT" });
            Add1212048(new User { Username = "123", Password = "111", Category = "abc" });
            Add1212048(new User { Username = "234", Password = "222", Category = "aaa" });
        }

        public IEnumerable<User> GetAll1212048()
        {
            return user;
        }

        public User Get1212048(string username)
        {
            return user.Single(p => p.Username == username);
        }

        public User Add1212048(User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            user.Add(item);
            return item;
        }

        public void Remove1212048(string username)
        {
            user.RemoveAll(p => p.Username == username);
        }

        public bool Update1212048(User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            //string index = user.Find( p => p . Username  == item . Username ).Username; 

            user.RemoveAll(p => p.Username == item.Username);
            user.Add(item);
            return true;
        } 
    }
}