﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212048.Models;

namespace WebAPI1212048.Controllers
{
    public class UsersController : ApiController
    {
        static readonly UserRepository repository = new UserRepository();

        [HttpGet]
        [ActionName("GET")]
        public IEnumerable<User> GetAllUser1212048()
        {
            return repository.GetAll1212048();
        }

        [HttpGet]
        [ActionName("GET")]
        public User GetUser1212048(string username)
        {
            User item = repository.Get1212048(username);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }

        //[HttpGet]
        //[ActionName("User")]
        public IEnumerable<User> GetUserByCategory1212048(string category)
        {
            return repository.GetAll1212048().Where(
                p => string.Equals(p.Category, category, StringComparison.OrdinalIgnoreCase));
        }

        [HttpPost]
        [ActionName("POST")]
        public User PostUser1212048(User item)
        {
            item = repository.Add1212048(item);
            return item;
        }

        [HttpPut]
        [ActionName("PUT")]
        public void PutUser1212048(string username, User item)
        {
            item.Username = username;
            if (!repository.Update1212048(item))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [AcceptVerbs("Del","HEAD")]
        public void DeleteUser1212048(string username)
        {
            User item = repository.Get1212048(username);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove1212048(username);
        }
    
    }
}
