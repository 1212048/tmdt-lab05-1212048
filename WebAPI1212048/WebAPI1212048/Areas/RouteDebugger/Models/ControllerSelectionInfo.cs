namespace WebAPI1212048.Areas.RouteDebugger.Models
{
    public class ControllerSelectionInfo
    {
        public string ControllerName { get; set; }

        public string ControllerType { get; set; }
    }
}
