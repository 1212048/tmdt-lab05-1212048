﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212048.Models
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll1212048();
        User Get1212048(string username);
        User Add1212048(User item);
        void Remove1212048(string username);
        bool Update1212048(User item);
    }
}
