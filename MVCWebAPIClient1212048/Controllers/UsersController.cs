﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using MVCWebAPIClient1212048.Models;

namespace MVCWebAPIClient1212048.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/

        public ActionResult Index()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:62347/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/2.0/users/get/").Result;
            if (response.IsSuccessStatusCode)
            {
                ViewBag.resuft = response.Content.ReadAsAsync<IEnumerable<Users>>().Result;
            }
            else
            {
                ViewBag.resuft = "Error";
            }
            return View();
        }

        [HttpGet]
        public ActionResult Search()
        {
            return View("Search");
        }

        [HttpPost]
        public ActionResult Search(FormCollection FC)
        {
             HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:62347/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/2.0/users/get/" + FC["Username"]).Result;
            if (response.IsSuccessStatusCode)
            {
                ViewBag.resuft = response.Content.ReadAsAsync<Users>().Result;
            }
            else
            {
                ViewBag.resuft = "Error";
            }
            return View("Search");
        }
    
    }
}
